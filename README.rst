#########################################################################################
Detecting presence of a signal by harmonising multi-omics high throughput sequencing data
#########################################################################################

.. Note::
   For more information please visit our poster_ on F1000 research's ABACBS_ collection. This poster was presented at the Australian Bioinformatics And Computational Biology Society Conference_ 2019.

.. _poster: https://f1000research.com/posters/8-2068
.. _ABACBS: https://f1000research.com/collections/abacbs
.. _Conference: https://www.abacbs.org/conference2019/about

Biological systems are highly complex and it is difficult to see how a stimulus leads to an outcome. To better understand these systems, it is first necessary to understand how they are regulated. However, it is also challenging to observe and interpret these hidden layers of regulatory mechanisms. One way to address this problem is to harmonise large quantities of curated biological experiments capturing information from different regulatory layers, with the combined biological data covering multiple different experimental types and modalities. From this data-driven approach, we **(1)** expect to identify weak signals in data and **(2)** observe signals which are not visible from one data source alone.

.. figure:: docs/images/readme/data_types.png
  :alt: Different biological data types and assays used to extract information from biological systems.
  :scale: 33 %

  Different biological data types and assays used to extract information from biological systems.

Here we show that our algorithm is able to perform a classification between DNA sequences enriched for the CTCF motif and DNA sequences without the CTCF motif.

.. figure:: docs/images/readme/CTCF.png
  :alt: CTCF motif

  CTCF binds to the consensus sequence CCGCGNGGNGGCAG and is involved in regulating many biological functions.

Furthermore, the method is agnostic to data modality, being effective on a combination of three different types of experimental data: ATAC-Seq, ChIP-Seq and RIP-Seq. The following series of genome browser tracks demonstrate this in assembly GRCh38:

.. figure:: docs/images/readme/chr1_248824218_248827614.png
  :alt: Chromosome 1: genomic coordinates 248824218-248827614
  :scale: 66 %

  Chromosome 1: genomic coordinates 248824218-248827614

.. figure:: docs/images/readme/chr17_77492442_77501529.png
  :alt: Chromosome 17: genomic coordinates 77492442_77501529
  :scale: 66 %

  Chromosome 17: genomic coordinates 77492442_77501529

.. figure:: docs/images/readme/chr19_55639002_55645470.png
  :alt: Chromosome 19: genomic coordinates 55639002_55645470
  :scale: 66 %

  Chromosome 19: genomic coordinates 55639002_55645470

.. figure:: docs/images/readme/chr22_19175164_19180710.png
  :alt: Chromosome 22: genomic coordinates 19175164-19180710
  :scale: 66 %

  Chromosome 22: genomic coordinates 19175164-19180710

Having shown that our method is effective on three different omics data types, we will next  expand the scope of this algorithm to other data types, particularly three dimensional chromosome conformation data.
